<?php

/**
 * @file
 * Exception classes for the Assembla bot.
 */

/**
 * @class
 * The general exception object for bot assembla errors.
 */
class BotAssemblaException extends Exception {
  /**
   * Log a message to the watchdog when throwing an exception.
   */
  function __construct($message, $variables = array(), $severity = WATCHDOG_NOTICE, $link = NULL) {
    watchdog('bot_assembla', $message, $variables, $severity, $link);
    $error = t($message, $variables);
    parent::__construct($error);
  }
}

/**
 * Specific exceptions to throw so that they can be caught if needed.
 */
class BotAssemblaHttpException extends BotAssemblaException {}
class BotAssemblaHttp404Exception extends BotAssemblaHttpException {}
class BotAssemblaSettingsException extends BotAssemblaException {}

