<?php

/**
 * @file
 * Administrative pages for the Bot Assembla module.
 */

/**
 * Configures Bot Assembla settings.
 */
function bot_assembla_settings() {
  $form['bot_assembla_enable'] = array(
    '#default_value' => variable_get('bot_assembla_enable', FALSE),
    '#title' => t('Enable Assembla ticket lookups'),
    '#type' => 'checkbox',
  );

  $form['bot_assembla_space'] = array(
    '#type' => 'textfield',
    '#title' => t('Assembla space'),
    '#default_value' => variable_get('bot_assembla_space', FALSE),
    '#required' => TRUE,
  );

  $form['bot_assembla_portal'] = array(
    '#type' => 'textfield',
    '#title' => t('Assembla portal subdomain'),
    '#default_value' => variable_get('bot_assembla_portal', FALSE),
  );

  $form['bot_assembla_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Assembla user name'),
    '#default_value' => variable_get('bot_assembla_user', FALSE),
  );

  $form['bot_assembla_password'] = array(
    '#type' => 'password',
    '#title' => t('Assembla password'),
    '#element_validate' => array('bot_assembla_password_validate'),
  );

  return system_settings_form($form);
}

/**
 * Form API #element_validate callback to keep the saved password if it isn't
 * changed.
 */
function bot_assembla_password_validate($element, &$form_state, $form) {
  if (empty($element['#value'])) {
    $form_state['values'][$element['#name']] = variable_get('bot_assembla_password', FALSE);
  }
}

